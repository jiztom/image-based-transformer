import torch.nn as nn

# Define the Custom Transformer Encoder Layer
class CustomTransformerEncoderLayer(nn.TransformerEncoderLayer):
    def __init__(self, *args, **kwargs):
        super(CustomTransformerEncoderLayer, self).__init__(*args, **kwargs)
        self.attention_weights = None

    def forward(self, src, *args, **kwargs):
        src2, attn_weights = self.self_attn(src, src, src, attn_mask=kwargs.get('attn_mask'), key_padding_mask=kwargs.get('key_padding_mask'))
        self.attention_weights = attn_weights
        src = src + self.dropout1(src2)
        src = self.norm1(src)
        src2 = self.linear2(self.dropout(self.activation(self.linear1(src))))
        src = src + self.dropout2(src2)
        src = self.norm2(src)
        return src

# Define the Vision Transformer Model
class VisionTransformer(nn.Module):
    def __init__(self, img_size_h=216, img_size_w=8, patch_size=4, in_channels=1, num_classes=1, dim=128, depth=6, heads=8, mlp_dim=256, dropout=0.1):
        super(VisionTransformer, self).__init__()
        assert img_size_h % patch_size == 0 and img_size_w % patch_size == 0, 'Image dimensions must be divisible by the patch size'
        num_patches = (img_size_h // patch_size) * (img_size_w // patch_size)  # For height 216 and width 8
        patch_dim = in_channels * patch_size ** 2

        self.patch_size = patch_size
        self.dim = dim

        self.patch_embeddings = nn.Linear(patch_dim, dim)
        self.positional_embeddings = nn.Parameter(torch.randn(1, num_patches + 1, dim))
        self.cls_token = nn.Parameter(torch.randn(1, 1, dim))
        self.dropout = nn.Dropout(dropout)

        self.transformer_layers = nn.ModuleList([
            CustomTransformerEncoderLayer(dim, heads, mlp_dim, dropout, batch_first=True) for _ in range(depth)
        ])

        self.to_cls_token = nn.Identity()
        self.mlp_head = nn.Sequential(
            nn.LayerNorm(dim),
            nn.Linear(dim, num_classes)
        )

    def forward(self, x):
        p = self.patch_size
        x = img_to_patch(x, p, p)  # Convert image to patches
        b, n, _ = x.size()
        print(f"Number of patches (n): {n}")
        cls_tokens = self.cls_token.expand(b, -1, -1)
        print(f"CLS tokens shape: {cls_tokens.shape}")
        x = torch.cat((cls_tokens, x), dim=1)
        print(f"Shape after concatenation: {x.shape}")
        x += self.positional_embeddings[:, :(n + 1)]
        x = self.dropout(x)

        for layer in self.transformer_layers:
            x = layer(x)

        x = self.to_cls_token(x[:, 0])
        return self.mlp_head(x)
