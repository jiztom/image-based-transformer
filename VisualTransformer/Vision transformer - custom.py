import numpy as np

from tqdm import tqdm, trange

import torch

import matplotlib.pyplot as plt
import numpy as np

np.random.seed(0)
torch.manual_seed(0)

import random
from torch.utils.data import Dataset
from torchvision import transforms
import torch.nn.functional as F
import torchvision

# Setting the seed
random.seed(42)
g = torch.Generator().manual_seed(2147483647)  # for reproducibility

# Ensure that all operations are deterministic on GPU (if used) for reproducibility
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

device = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
print("Device:", device)

# Fixed values
image_size = 250
embed_dim=256
hidden_dim=embed_dim*3
num_heads=8
num_layers=6
patch_size=50
num_patches=16
num_channels=1
num_classes=10
dropout=0.2

model_save_path = r'/home/jiztom/PycharmProjects/image-based-transformer/VisualTransformer/Model_save'
epoch_range = 50

patch_size_h = 50  # Example patch height
patch_size_w = 7  # Example patch width


train_norm = r'/home/jiztom/PycharmProjects/image-based-transformer/Data/CropYield/Cleaned/Cleaned/norm_train_02292024.npy'
train_yield = r'/home/jiztom/PycharmProjects/image-based-transformer/Data/CropYield/Cleaned/Cleaned/norm_yield_train.npy'

#%%
data_train = np.load(train_norm)
data_tensor = torch.from_numpy(data_train)
data_tensor = data_tensor.permute(2, 1, 0)

train_label = np.load(train_yield)
label_tensor = torch.from_numpy(train_label)

class CustomTensorDataset(Dataset):

    def __init__(self, data_tensor, labels_tensor, transform=None):
        self.data_tensor = data_tensor
        self.labels_tensor = labels_tensor
        self.transform = transform


    def __len__(self):
        return len(self.data_tensor)


    def __getitem__(self, idx):
        sample = self.data_tensor[idx]
        label = self.labels_tensor[idx]

        if self.transform:
            sample = self.transform(sample)

        return sample, label


# Define any required transformations
transform = transforms.Compose([
    # Add any transformations you need here
])

from torch.utils.data import Dataset, DataLoader
dataset = CustomTensorDataset(data_tensor=data_tensor, labels_tensor=label_tensor, transform=transform)
dataloader = DataLoader(dataset, batch_size=32, shuffle=True, num_workers=4)

# print the dimension of images to verify all loaders have the same dimensions
def print_dim(loader, text):
  print('---------'+text+'---------')
  print(len(loader.dataset))
  for image, label in loader:
    print(image.shape)
    print(label.shape)
    break

print_dim(dataloader,'training loader')

# Example data: creating multiple images with dimensions 214x7
num_images = 4
# images = [dataset[idx] for _ in range(num_images)]

# Plot the images with gradients
fig, axes = plt.subplots(1, num_images, figsize=(10, 2))  # Create subplots

for i, ax in enumerate(axes):
    img = dataset[i][0].transpose(1, 0)
    cax = ax.imshow(img, aspect='auto', cmap='viridis')
    ax.set_title(f"Image {i+1}")
    ax.set_xlabel("Width (7 pixels)")
    ax.set_ylabel("Height (214 pixels)")
    fig.colorbar(cax, ax=ax, orientation='vertical')  # Add color bar to each subplot

plt.suptitle("Multiple Images with Gradients")
plt.tight_layout()
plt.show()


def img_to_non_square_patch(x, patch_size_h, patch_size_w, flatten_channels=True):
    B, C, H, W = x.shape
    x = x.reshape(B, C, H // patch_size_h, patch_size_h, W // patch_size_w, patch_size_w)
    x = x.permute(0, 2, 4, 1, 3, 5)
    x = x.flatten(1, 2)
    if flatten_channels:
        x = x.flatten(2, 4)
    return x

# Example usage
image = torch.randn(1, 1, 214, 7)  # Example image tensor with shape [B, C, H, W]
patch_size_h = 107  # Choose a patch height that is a factor of 214
patch_size_w = 7  # Choose a patch width that is a factor of 7
patches = img_to_non_square_patch(image, patch_size_h, patch_size_w)
print(f"Patches shape: {patches.shape}")


def pad_image(image, patch_size_h, patch_size_w):
    B, C, H, W = image.shape
    pad_h = (patch_size_h - (H % patch_size_h)) % patch_size_h
    pad_w = (patch_size_w - (W % patch_size_w)) % patch_size_w

    padding = (0, pad_w, 0, pad_h)  # (left, right, top, bottom)
    padded_image = F.pad(image, padding)
    return padded_image


image, first_label = next(iter(dataloader))
patch_size_h = 50  # Example patch height
patch_size_w = 7  # Example patch width
padded_image = pad_image(image.unsqueeze(0), patch_size_h, patch_size_w)
print(f"Padded image shape: {padded_image.shape}")

# Now, define patches for the padded image
patches = img_to_non_square_patch(padded_image, patch_size_h, patch_size_w,flatten_channels=False)
print(f"Patches shape: {patches.shape}")

NUM_IMAGES = 4
examples = torch.stack([dataset[idx][0] for idx in range(NUM_IMAGES)], dim=0)
img_grid = torchvision.utils.make_grid(examples, nrow=2, normalize=True, pad_value=0.9)
img_grid = img_grid.permute(1, 2, 0)

plt.figure(figsize=(8, 8))
plt.title("Image examples of the MNIST dataset")
plt.imshow(img_grid.transpose(0,1))
plt.axis("off")
plt.show()
plt.close()

