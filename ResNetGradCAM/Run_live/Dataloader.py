from PIL import Image
from torch.utils.data import Dataset


def init(show_image_size_init):
    global show_image_size
    show_image_size =show_image_size_init


class CustomImageDataset(Dataset):
    '''
    Custom dataloader. This will work only for the Crop Yeild Dataset we have in store.
    '''

    def __init__(self, annotations_file, transform=None, target_transform=None):
        # self.img_labels = annotations_file['Label']
        # self.img_dir = annotations_file['FilePath']
        self.img_dir = annotations_file['Data']
        self.transform = transform
        self.target_transform = target_transform
        self.yield_data = annotations_file['Yield_Data']

    def __len__(self):
        # return len(self.yield_data)
        return len(self.yield_data)

    def __getitem__(self, idx):
        global show_image_size
        # img_path = self.img_dir[idx]
        # print(img_path, self.img_labels[idx])
        # image = Image.open(img_path)
        # image_temp = Image.fromarray((image_crop(img_path)))
        image_temp = Image.fromarray(self.img_dir[idx])
        # image = transform.to_tensor(image_temp)
        if show_image_size:
            print(image_temp.size, end="\r")
        image = image_temp

        # image = self.image_crop(img_path)
        # label = self.img_labels.iloc[idx]

        if self.transform:
            image = self.transform(image)
        yield_data = self.yield_data[idx]
        return image, yield_data



