# Importing torch and the related packages
import torch
from torch import nn


# Importing support packages
import pandas as pd

from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error as mae
from sklearn.metrics import mean_squared_error

import matplotlib.pyplot as plt

import numpy as np
import suppliment
from sys import exit


def init(step_init, loss_func_name_init, val_step_init, device_init, writer_init, normalize_init, Debug_init):
    global step, loss_func_name, val_step, device, writer, normalize,Debug
    step = step_init
    loss_func_name = loss_func_name_init
    val_step = val_step_init
    device = device_init
    writer = writer_init
    normalize =normalize_init
    Debug = Debug_init


class RMSELoss(nn.Module):
    def __init__(self, eps=1e-6):
        super().__init__()
        self.mse = nn.MSELoss()
        self.eps = eps

    def forward(self, yhat, y):
        loss = torch.sqrt(self.mse(yhat, y) + self.eps)
        return loss


def train_loop(dataloader, test_dataloader, model, loss_fn, optimizer, epoch, minMax):
    global step, loss_func_name, val_step, device, writer, normalize
    size = len(dataloader.dataset)
    ep_loss = []
    ep_accuracy = []

    y_ground = []
    y_pred = []

    for batch, (X, y) in enumerate(dataloader):
        # Compute prediction and loss
        # X = X.to(device).squeeze(1)
        X = X.to(device)
        y = y.to(device)
        # print(X.shape)
        # exit()
        pred = model(X)

        # pred = pred.squeeze(-1)
        # print(pred.shape)
        # exit()

        # print("this is the shape of both", y.shape, pred.shape)
        if loss_func_name == 'MSEloss' or 'RMSEloss':
            loss = loss_fn(pred, y.to(torch.float32))
        else:
            loss = loss_fn(pred, y)

        if normalize:
            norm_loss = (loss * (minMax['max'] - minMax['min'])) + minMax['min']
        else:
            norm_loss = loss

        accuracy = suppliment.akkuracy(y, pred)

        if not Debug:
            writer.add_scalar('Training/Loss', norm_loss, step)
            writer.add_scalar('Training/Accuracy', accuracy, step)

        step += 1
        ep_loss.append(loss.item())
        ep_accuracy.append(accuracy)

        y_ground.extend(y.to('cpu').detach().numpy())
        y_pred.extend(pred.to('cpu').detach().numpy())

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(X)
            denorm_loss = (loss * (minMax['max'] - minMax['min'])) + minMax['min']
            if loss_func_name == 'MSEloss':
                if normalize:
                    # print(f"loss: {loss:>7f}  RMSE : {np.sqrt(loss):>7f} | loss(norm): {denorm_loss:>7f} "
                    #       f"RMSE(norm): {np.sqrt(denorm_loss):>7f} | accuracy: {accuracy:>7f} "
                    #       f"[{current:>5d}/{size:>5d}]")
                    print(f"loss: {loss:>7f}  RMSE : {np.sqrt(loss):>7f} | accuracy: {accuracy:>7f} "
                          f"[{current:>5d}/{size:>5d}]")
                else:
                    print(f"loss: {loss:>7f}  RMSE : {np.sqrt(loss):>7f} | accuracy: {accuracy:>7f} "
                          f"[{current:>5d}/{size:>5d}]")
            else:
                print(f"loss: {loss:>7f} | n-loss: {denorm_loss:>7f} | accuracy: {accuracy:>7f}"
                      f"[{current:>5d}/{size:>5d}]")
    r2_train = r2_score(y_ground, y_pred)
    keys = ['ActualGround', 'ActualPred']
    train_dict = dict.fromkeys(keys)
    train_dict['ActualGround'] = y_ground
    train_dict['AcutalPred'] = y_pred

    train_df = pd.DataFrame(train_dict)
    train_df['ActualGround'] = train_df['ActualGround'].apply(suppliment.denormalized,
                                                             args=(minMax['min'], minMax['max']))
    train_df['ActualPred'] = train_df['AcutalPred'].apply(suppliment.denormalized, args=(minMax['min'], minMax['max']))

    MSE_actual_train = mean_squared_error(train_df['ActualGround'], train_df['ActualPred'])
    RMSE_actual_train = np.sqrt(MSE_actual_train)
    MAE_actual_train = mae(train_df['ActualGround'], train_df['ActualPred'])

    with torch.no_grad():

        val_epoch_loss = []
        val_epoch_accuracy = []
        rms = 0
        model.eval()
        data_ground = []
        data_pred = []
        for X_val_batch, y_val_batch in test_dataloader:
            X_val_batch, y_val_batch = X_val_batch.to(device), y_val_batch.to(device)

            y_val_pred = model(X_val_batch)

            # val_loss = loss_fn(y_val_pred, y_val_batch.unsqueeze(1))
            if loss_func_name == 'MSEloss':
                val_loss = loss_fn(y_val_pred, y_val_batch.to(torch.float32))
            else:
                val_loss = loss_fn(y_val_pred, y_val_batch)

            if normalize:
                norm_val_loss = (val_loss * (minMax['max'] - minMax['min'])) + minMax['min']
            else:
                norm_val_loss = val_loss

            # conf_matrix = confusion_matrix(y_true=y_test, y_pred=y_pred)

            val_accuracy = suppliment.akkuracy(y_val_batch, y_val_pred)

            data_ground.extend(y_val_batch.to('cpu').numpy())
            data_pred.extend(y_val_pred.to('cpu').numpy())

            if not Debug:
                writer.add_scalar('Evaluation/Loss', norm_val_loss, val_step)
                writer.add_scalar('Evaluation/Accuracy', val_accuracy, val_step)
            val_step += 1

            val_epoch_loss.append(val_loss.item())
            val_epoch_loss.append(val_loss.item())
            val_epoch_accuracy.append(val_accuracy)

    r2_val = r2_score(data_ground, data_pred)

    val_dict = dict.fromkeys(keys)
    val_dict['ActualGround'] = data_ground
    val_dict['ActualPred'] = data_pred

    val_df = pd.DataFrame(val_dict)

    val_df['ActualGround'] = val_df['ActualGround'].apply(suppliment.denormalized, args=(minMax['min'], minMax['max']))
    val_df['ActualPred'] = val_df['ActualPred'].apply(suppliment.denormalized, args=(minMax['min'], minMax['max']))

    MSE_actual_val = mean_squared_error(val_df['ActualGround'], val_df['ActualPred'])
    RMSE_actual_val = np.sqrt(MSE_actual_val)
    MAE_actual_val = mae(val_df['ActualGround'], val_df['ActualPred'])

    epoch_loss_avg = sum(ep_loss) / len(ep_loss)
    val_epoch_loss_avg = sum(val_epoch_loss) / len(val_epoch_loss)

    # if normalize:
    #     epoch_loss_avg = (epoch_loss_avg * (minMax['max'] - minMax['min'])) + minMax['min']
    #     val_epoch_loss_avg = (val_epoch_loss_avg * (minMax['max'] - minMax['min'])) + minMax['min']

    if loss_func_name == 'MSEloss':
        print(
            f'{loss_func_name} - Epoch {epoch + 1}: | Train Loss: MSE - {epoch_loss_avg:>7f} '
            f'RMSE - {np.sqrt(epoch_loss_avg):>7f} | '
            f'Val Loss: MSE - {val_epoch_loss_avg:>7f} '
            f'RMSE {np.sqrt(val_epoch_loss_avg):>7f} ')
    else:
        print(
            f'{loss_func_name} - Epoch {epoch + 1}: | Train Loss: {epoch_loss_avg:>7f} | '
            f'Val Loss: {val_epoch_loss_avg:>7f} ')
    print(f'R2 value  \t Train : {r2_train} | Validation : {r2_val}')
    print(f'Training \t RMSE : {RMSE_actual_train} MAE : {MAE_actual_train} \n'
          f'Validation \t RMSE : {RMSE_actual_val} MAE : {MAE_actual_val}')
    data_val = [RMSE_actual_train, MAE_actual_train, RMSE_actual_val, MAE_actual_val]
    return ep_loss, ep_accuracy, val_epoch_loss, val_epoch_accuracy, r2_train, r2_val, data_val
