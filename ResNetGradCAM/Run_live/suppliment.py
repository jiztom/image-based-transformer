import torch
import pandas as pd


def init(pct_close_init):
    global pct_close
    pct_close = pct_close_init


def akkuracy(data_y, pred):
    global pct_close

    # pure Tensor, efficient version
    n_items = len(data_y)
    Y = data_y  # actual as [102] Tensor
    # predicted as [102,1] Tensor
    pred = pred.view(n_items)  # predicted as [102]

    n_correct = torch.sum((torch.abs(pred - Y) < torch.abs(pct_close * Y)))
    acc = (n_correct.item() * 100.0 / n_items)  # scalar
    return acc


def minMax(x):
    return pd.Series(index=['min', 'max'], data=[x.min(), x.max()])


def normalized(x, min, max):
    return (x - min) / (max - min)


def denormalized(x, min, max):
    return (x * (max - min)) + min
