# Importing torch and the related packages
import torch
from torch import nn
from torch.utils.data import Dataset

# Importing torch vision model
import torchvision
import torchvision.models as models
import torchvision.transforms as transforms


# from torchvision.io import read_image


def init(pretrain_init, model_structure_init):
    global pretrain, model_structure
    pretrain = pretrain_init
    model_structure = model_structure_init


class AdaptiveConcatPool2d(nn.Module):
    def __init__(self, sz=None):
        super().__init__()
        sz = sz or (1, 1)
        self.ap = nn.AdaptiveAvgPool2d(sz)
        self.mp = nn.AdaptiveMaxPool2d(sz)

    def forward(self, x):
        return torch.cat([self.mp(x), self.ap(x)], 1)


class YieldModel(nn.Module):
    global pretrain, model_structure

    def __init__(self):
        super().__init__()
        layers = []
        if model_structure == 'ResNext50':
            layers = list(models.resnext50_32x4d(pretrained=pretrain).children())[:-2]
        else:
            layers = list(models.resnet34(pretrained=pretrain).children())[:-2]
        layers[0] = nn.Conv2d(1, 64, kernel_size=(214, 7), stride=(2, 2), padding=(3, 3), bias=False)
        layers += [AdaptiveConcatPool2d(), nn.Flatten()]

        if model_structure == 'ResNet50':
            layers += [nn.BatchNorm1d(4096, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)]
            layers += [nn.Dropout(p=0.60)]
            layers += [nn.Linear(4096, 1024, bias=True), nn.ReLU(inplace=True)]
            layers += [nn.Dropout(p=0.50)]

        layers += [nn.BatchNorm1d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)]
        layers += [nn.Dropout(p=0.50)]
        layers += [nn.Linear(1024, 512, bias=True), nn.ReLU(inplace=True)]
        layers += [nn.BatchNorm1d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)]
        layers += [nn.Dropout(p=0.50)]
        layers += [nn.Linear(512, 16, bias=True), nn.ReLU(inplace=True)]
        layers += [nn.Linear(16, 1)]
        self.YieldModel = nn.Sequential(*layers)

    def forward(self, x):
        return self.YieldModel(x).squeeze(-1)
        # could add 116*torch.sigmoid


if __name__ == '__main__':
    # Deciding which device to used
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    # device = 'cpu'
    print('Using {} device'.format(device))

    global pretrain, model_structure
    pretrain = False
    model_structure = 'Resnet34'

    model = YieldModel().to(device)
    print(model)
